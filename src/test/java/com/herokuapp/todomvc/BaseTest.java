package com.herokuapp.todomvc;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static com.herokuapp.todomvc.core.ConciseAPI.getWebDriver;
import static com.herokuapp.todomvc.core.ConciseAPI.setWebDriver;

/**
 * Created by aru on 2017-11-02.
 */
public class BaseTest {

    @BeforeClass
    public static void initializeDriver(){
        setWebDriver(new ChromeDriver());
    }

    @AfterClass
    public static void tearDown(){
        getWebDriver().quit();
    }
}

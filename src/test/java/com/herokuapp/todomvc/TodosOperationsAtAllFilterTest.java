package com.herokuapp.todomvc;

import com.herokuapp.todomvc.pages.ToDoMVC;
import org.junit.Test;

import static com.herokuapp.todomvc.pages.ToDoMVC.TaskStatus.ACTIVE;
import static com.herokuapp.todomvc.pages.ToDoMVC.TaskStatus.COMPLETED;
import static com.herokuapp.todomvc.pages.ToDoMVC.aTask;

/**
 * Created by aru on 2017-11-10.
 */
public class TodosOperationsAtAllFilterTest extends BaseTest{
/*
только в feature tests падает edit:
org.openqa.selenium.UnsupportedCommandException: mouseMoveTo
Build info: version: '3.4.0', revision: 'unknown', time: 'unknown'
System info: host: '63MBZ52', ip: '172.20.1.198', os.name: 'Windows 10', os.arch: 'amd64', os.version: '10.0', java.version: '1.8.0_121'

 */
    @Test
    public void testEdit() {
        ToDoMVC.given(aTask(ACTIVE, "1"));

        ToDoMVC.edit("1", "1_edited");
        ToDoMVC.assertTasksAre("1_edited");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testDelete() {
        ToDoMVC.given(aTask(ACTIVE, "1", "2"));

        ToDoMVC.delete("2");
        ToDoMVC.assertTasksAre("1");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptying() {
        ToDoMVC.given(aTask(ACTIVE, "1", "2"));

        ToDoMVC.edit("2", "");
        ToDoMVC.assertTasksAre("1");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testCompleteAll() {
        ToDoMVC.given(aTask(ACTIVE, "1"), aTask(COMPLETED, "2"));

        ToDoMVC.toggleAll();
        ToDoMVC.assertTasksAre("1", "2");
        ToDoMVC.assertItemsLeft(0);
    }

    @Test
    public void testFilteringActiveToAll() {
        ToDoMVC.givenAtActive(aTask(COMPLETED, "1"), aTask(ACTIVE, "2"));

        ToDoMVC.filterAll();
        ToDoMVC.assertTasksAre("1", "2");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testClearCompleted() {
        ToDoMVC.given(aTask(COMPLETED, "1", "2"));

        ToDoMVC.clearCompleted();
        ToDoMVC.assertNoTasks();
        ToDoMVC.assertNoFooter();
    }

    @Test
    public void testCancelEdit() {
        ToDoMVC.given(aTask(ACTIVE, "1", "2"));

        ToDoMVC.cancelEdit("1", "1_edited");
        ToDoMVC.assertTasksAre("1", "2");

    }

    @Test
    public void testReopen() {
        ToDoMVC.given(aTask(COMPLETED, "1", "2"));

        ToDoMVC.toggle("2");
        ToDoMVC.assertTasksAre("1", "2");
        ToDoMVC.assertItemsLeft(1);
    }
}



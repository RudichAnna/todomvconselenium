package com.herokuapp.todomvc;

import com.herokuapp.todomvc.pages.ToDoMVC;
import org.junit.Test;


/**
 * Created by aru on 2017-11-01.
 */

public class TodosE2ETest extends BaseTest {

   @Test
   public void lifeCycle_1() {

      ToDoMVC.given();

      ToDoMVC.add("1");
      ToDoMVC.toggle("1");//completeAtAll
      ToDoMVC.assertTasksAre("1");

      ToDoMVC.filterActive();//AllSuiteTest->Active
      ToDoMVC.assertNoTasks();
      ToDoMVC.add("2");
      ToDoMVC.assertTasksAre("2");
      ToDoMVC.toggleAll();//completeAllAtActive
      ToDoMVC.assertNoTasks();

      ToDoMVC.filterCompleted();//Active->Completed
      ToDoMVC.assertTasksAre("1", "2");
      ToDoMVC.assertItemsLeft(0);
      ToDoMVC.toggle("2");//reopenAtCompleted
      ToDoMVC.clearCompleted();
      ToDoMVC.assertNoTasks();

      ToDoMVC.filterAll();//Completed->AllSuiteTest
      ToDoMVC.assertTasksAre("2");
   }
}



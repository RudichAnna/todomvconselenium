package com.herokuapp.todomvc.core;

import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aru on 2017-11-10.
 */
class Helpers {//доступ в пределах пакета вроде ок?

    static List<WebElement> getVisibleElements(List<WebElement> elements) {
        List<WebElement> visibleElements = new ArrayList<>();

        for (WebElement element : elements) {
            if (element.isDisplayed()) {
                visibleElements.add(element);
            }
        }
        return visibleElements;
    }

    static List<String> getTexts(List<WebElement> elements) {
        List<String> texts = new ArrayList<>();

        for (WebElement element : elements) {
            texts.add(element.getText());
        }
        return texts;
    }
}

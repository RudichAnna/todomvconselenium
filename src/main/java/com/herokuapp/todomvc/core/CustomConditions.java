package com.herokuapp.todomvc.core;

import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.herokuapp.todomvc.core.Helpers.*;

/**
 * Created by aru on 2017-11-02.
 */
public class CustomConditions {

    public static ExpectedCondition<WebElement> listElementWithText(final By locator, final String elementText) {

        return elementExceptionsCatcher(new ExpectedCondition<WebElement>() {
            List<String> actualTexts = new ArrayList<>();

            public WebElement apply(WebDriver driver) {
                List<WebElement> elements = driver.findElements(locator);
                actualTexts=getTexts(elements);

                for (int i = 0; i < actualTexts.size(); i++) {
                    if (actualTexts.get(i).equals(elementText)) {
                        return elements.get(i);
                    }
                }
                return null;
            }

            public String toString() {
                return String.format(
                        "List of elements located by %s\n" +
                                "should contain element with text %s\n" +
                                "while actual texts are %s\n", locator, elementText, actualTexts);
            }
        });
    }


    public static ExpectedCondition<WebElement> listElementWithCssClass(final By locator, final String cssClass) {

        return elementExceptionsCatcher(new ExpectedCondition<WebElement>() {
            List<String> actualClasses = new ArrayList<>();

            public WebElement apply(WebDriver driver) {
                List<WebElement> elements = driver.findElements(locator);

                actualClasses.clear();
                for (WebElement element : elements) {
                    actualClasses.add(element.getAttribute("class"));
                }

                for (int i = 0; i < actualClasses.size(); i++) {
                    if (Arrays.asList(actualClasses.get(i).split(" ")).contains(cssClass)) {//в списке ищет элемент в списке равный тому что я передала
                        return elements.get(i);
                    }
                }
                return null;
            }

            public String toString() {
                return String.format(
                        "List of elements located by %s\n" +
                                "should contain element with  cssClass %s\n" +
                                "while actual classes are %s\n", locator, cssClass, actualClasses);
            }
        });
    }


    public static ExpectedCondition<WebElement> listNthElementHasText(final By locator, final int index, final String expectedText) {

        return elementExceptionsCatcher(new ExpectedCondition<WebElement>() {
            private String actualText;


            public WebElement apply(WebDriver driver) {
                WebElement element = driver.findElements(locator).get(index);

                actualText = element.getText();

                if (!actualText.contains(expectedText)) {
                    return null;
                }
                return element;
            }

            public String toString() {
                return String.format("element with index: %s of the list located by %s\n" +
                                "should have text:%s\n " +
                                "while actual text was: %s\n",
                        index, locator, expectedText, actualText);
            }
        });
    }

    public static ExpectedCondition<List<WebElement>> textsOf(final By locator, final String... expectedTexts) {
        if (expectedTexts.length == 0) {
            throw new IllegalArgumentException("Array of expected texts is empty.");
        }

        return elementExceptionsCatcher(new ExpectedCondition<List<WebElement>>() {
            List<String> actualTexts = new ArrayList<>();

            public List<WebElement> apply(WebDriver driver) {
                List<WebElement> elements = driver.findElements(locator);
                //actualTexts.clear();
                actualTexts = getTexts(elements);

                if (actualTexts.size() != expectedTexts.length) {
                    return null;
                }
                for (int i = 0; i < expectedTexts.length; i++) {
                    if (!actualTexts.get(i).contains(expectedTexts[i])) {
                        return null;
                    }
                }
                return elements;
            }

            public String toString() {
                return String.format("\n size of list of webElements found by locator%s\n" +
                        "should be: %s\n" +
                        "while actual texts are: %s\n", locator, Arrays.asList(expectedTexts), actualTexts);
            }
        });
    }

    public static ExpectedCondition<List<WebElement>> textsOfVisible(final By locator, final String... expectedTexts) {//sizeOfVisible 173-178 expectedSize
        if (expectedTexts.length == 0) {
            throw new IllegalArgumentException("Array of expected texts is empty.");
        }

        return elementExceptionsCatcher(new ExpectedCondition<List<WebElement>>() {
            List<String> actualTexts = new ArrayList<>();

            public List<WebElement> apply(WebDriver driver) {
                List<WebElement> visibleElements = getVisibleElements(driver.findElements(locator));

                //actualTexts.clear();
                actualTexts = getTexts(visibleElements);

                if (actualTexts.size() != expectedTexts.length) {
                    return null;
                }
                for (int i = 0; i < expectedTexts.length; i++) {
                    if (!actualTexts.get(i).contains(expectedTexts[i])) {
                        return null;
                    }
                }
                return visibleElements;
            }

            public String toString() {
                return String.format("\n texts of visible elements of list found by locator %s\n" +
                        "should be: %s\n" +
                        "while actual texts are: %s\n", locator, Arrays.asList(expectedTexts), actualTexts);
            }
        });
    }

    public static ExpectedCondition<List<WebElement>> sizeOfVisible(final By locator, final int expectedSize) {

        return elementExceptionsCatcher(new ExpectedCondition<List<WebElement>>() {
            int actualSize = 0;

            public List<WebElement> apply(WebDriver driver) {
                List<WebElement> visibleElements = getVisibleElements(driver.findElements(locator));
                actualSize=visibleElements.size();

                if (actualSize != expectedSize) {
                    return null;
                }
                return visibleElements;
            }

            public String toString() {
                return String.format("size of visible elements in the list found by locator %s\n" +
                        "should be: %s\n" +
                        "while actual size is: %s\n", locator, expectedSize, actualSize);
            }
        });
    }


    public static ExpectedCondition<List<WebElement>> exactTextsOf(final By locator, final String... expectedTexts) {
        if (expectedTexts.length == 0) {
            throw new IllegalArgumentException("Array of expected texts is empty.");
        }

        return elementExceptionsCatcher(new ExpectedCondition<List<WebElement>>() {
            List<String> actualTexts = new ArrayList<>();

            public List<WebElement> apply(WebDriver driver) {
                List<WebElement> elements = driver.findElements(locator);
                actualTexts.clear();
                actualTexts = getTexts(elements);

                if (actualTexts.size() != expectedTexts.length) {
                    return null;
                }
                for (int i = 0; i < expectedTexts.length; i++) {
                    if (!actualTexts.get(i).equals(expectedTexts[i])) {
                        return null;
                    }
                }
                return elements;
            }

            public String toString() {
                return String.format("\n texts of list of webElements found by locator%s\n" +
                        "should be: %s\n" +
                        "while actual texts are: %s\n", locator, Arrays.asList(expectedTexts), actualTexts);
            }
        });
    }

    /*
    assertThat(jsReturnedTrue(
            "return " +
            "$._data($('#new-todo').get(0), 'events').hasOwnProperty('keyup')&& "+
            "$._data($('#toggle-all').get(0), 'events').hasOwnProperty('change') && " +
            "$._data($('#clear-completed').get(0), 'events').hasOwnProperty('click')"));
     */
    public static ExpectedCondition<Boolean> jsReturnedTrue(final String script, final Object... arguments) {

        return new ExpectedCondition<Boolean>() {
            Object result;

            public Boolean apply(WebDriver webDriver) {
                try {
                    this.result = ((JavascriptExecutor) webDriver).executeScript(script, arguments);
                    return (Boolean) this.result;
                } catch (Exception e) {
                    return false;
                }
            }

            public String toString() {
                return String.format(
                        "expected script: %s\n" +
                                "to return: true\n" +
                                "but returned: %s\n",
                        script, this.result
                );
            }
        };
    }


    private static <V> ExpectedCondition<V> elementExceptionsCatcher(final Function<? super WebDriver, V> condition) {
        return new ExpectedCondition<V>() {
            public V apply(WebDriver input) {
                try {
                    return condition.apply(input);
                } catch (StaleElementReferenceException | ElementNotVisibleException | IndexOutOfBoundsException e) {
                    return null;
                }
            }

            public String toString() {
                return condition.toString();
            }
        };
    }
}

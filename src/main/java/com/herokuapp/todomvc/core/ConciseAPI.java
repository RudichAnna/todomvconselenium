package com.herokuapp.todomvc.core;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Quotes;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;


/**
 * Created by aru on 2017-11-02.
 */
public class ConciseAPI {

    private static WebDriver driver;

    private static Actions actions() {
        return new Actions(getWebDriver());
    }


    public static void setWebDriver(WebDriver driver) {
        File chromeDriver = new File("src/main/resources/drivers/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());
        ConciseAPI.driver = driver;
    }

    public static WebDriver getWebDriver() {
        return driver;
    }

    public static void executeJS(String script) {
        ((JavascriptExecutor) getWebDriver()).executeScript(script);
    }


    public static WebElement doubleClick(WebElement element) {
        actions().doubleClick(element).perform();
        return element;
    }

    public static void hover(WebElement element) {
        actions().moveToElement(element).perform();
    }

    public static void refresh() {
        driver.navigate().refresh();
    }

    public static void sleep(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    public static void openUrl(String url) {
        getWebDriver().get(url);
    }

    public static String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public static WebElement $(By locator) {
        return assertThat(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static WebElement $(String cssSelector) {
        return assertThat(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssSelector)));
    }


    public static By byText(String elementText) {
        return By.xpath(".//*/text()[normalize-space(.) = " + Quotes.escape(elementText) + "]/parent::*");
    }

    public static WebElement $(ExpectedCondition<WebElement> conditionToWaitParentElement, By innerElementLocator) {
        return assertThat(conditionToWaitParentElement).findElement(innerElementLocator);
    }

    public static WebElement $(ExpectedCondition<WebElement> conditionToWaitParentElement, String innerElementCssSelector) {
        return assertThat(conditionToWaitParentElement).findElement(By.cssSelector(innerElementCssSelector));
    }

    public static WebElement setValue(WebElement element, String value) {
        element.clear();
        element.sendKeys(value);
        return element;
    }

    public static WebElement setValue(By elementLocator, String value) {
        $(elementLocator).clear();
        $(elementLocator).sendKeys(value);
        return $(elementLocator);
    }

    public static void setValue(String cssSelector, String value) {
        setValue(By.cssSelector(cssSelector), value);
    }

    public static <V> V assertThat(ExpectedCondition<V> condition, long timeout) {
        return new WebDriverWait(driver, timeout).until(condition);
    }

    public static <V> V assertThat(ExpectedCondition<V> condition) {
        return assertThat(condition, Configuration.timeout);
    }
}

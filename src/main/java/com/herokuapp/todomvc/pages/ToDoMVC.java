package com.herokuapp.todomvc.pages;

import com.herokuapp.todomvc.core.CustomConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.StringJoiner;

import static com.herokuapp.todomvc.core.ConciseAPI.*;
import static com.herokuapp.todomvc.core.CustomConditions.listElementWithText;
import static com.herokuapp.todomvc.pages.ToDoMVC.TaskStatus.*;


/**
 * Created by aru on 2017-11-01.
 */
public class ToDoMVC{


    private static By newTodo = By.cssSelector("#new-todo");

    private static By tasks = By.cssSelector("#todo-list>li");

    //how to improve?
    /*
    Ну да - тут есть момент)
    я в конце ревью пропишу - что тут за проблемка
    самый простой костыль - перед циклом встывь sleep на одну-две секунды

    к сожалению - тут дождатья enabled - будет недостаточно
    эту проверку можешь убирать
     */
    public static void add(String... taskTexts){
        //sleep(2000);
        for (String text: taskTexts) {
            setValue(newTodo, text + Keys.ENTER);
        }
    }

    public static WebElement startEdit(String oldTaskText, String newTaskText) {
        doubleClick($(listElementWithText(tasks, oldTaskText), "label"));//нагородила )
        WebElement inputEdit = $(CustomConditions.listElementWithCssClass (tasks, "editing"), ".edit");
        return setValue(inputEdit, newTaskText);
    }

    public static  void edit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).sendKeys(Keys.ENTER);
    }

    public static  void editByClickOutSide(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText);
        $(newTodo).click();
    }

    public static  void cancelEdit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).sendKeys(Keys.ESCAPE);
    }

    public static  void editByTab(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).sendKeys(Keys.TAB);
    }

    public static  void delete(String taskText) {
        hover($(byText(taskText)));
        $(listElementWithText(tasks, taskText), ".destroy").click();
    }

    public static  void toggle(String taskText) {


        $(listElementWithText(tasks, taskText), ".toggle").click();
    }

    public static  void toggleAll() {
        $("#toggle-all").click();
    }

    public static  void clearCompleted() {
        $("#clear-completed").click();
    }

    public static  void filterActive() {
        $(By.linkText("Active")).click();
    }

    public static  void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    public static  void filterAll() {
        $(By.linkText("All")).click();
    }

    public static  void assertTasksAre(String... taskTexts) {
        assertThat(CustomConditions.textsOfVisible(tasks, taskTexts));
    }

    public static  void assertNoTasks() {
        assertThat(CustomConditions.sizeOfVisible(tasks, 0));//waiting for number to be "0". Current number: "1" sizeOfVisible()
    }

    public static  void assertItemsLeft(int count) {
        assertThat(ExpectedConditions.textToBe(By.cssSelector("#todo-count>strong"), Integer.toString(count)));
    }

    public static  void assertNoFooter() {
        assertThat(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#footer")));
    }


    public static  enum TaskStatus {
        ACTIVE, COMPLETED
    }


    public static class Task {
        String text;
        TaskStatus status;

        Task(TaskStatus status, String text) {
            this.text = text;
            this.status = status;
        }

        public String toString() {
            String taskStatus = (status == ACTIVE) ? "false" : "true";
            return "{\\\"completed\\\":" + taskStatus + ", \\\"title\\\":\\\"" + text + "\\\"}";
        }

    }

    public static  void given(Task... tasks) {
        ensureUrlIsCorrect();

        StringJoiner addTasksJs = new StringJoiner(",", "localStorage.setItem(\"todos-troopjs\", \"[", "]\")");

        for (Task task : tasks) {
            addTasksJs.add(task.toString());
        }

        String result = addTasksJs.toString();

        executeJS(result);

        refresh();

        assertThat(CustomConditions.jsReturnedTrue(
                "return " +
                        "$._data($('#new-todo').get(0), 'events').hasOwnProperty('keyup')&& "+
                        "$._data($('#toggle-all').get(0), 'events').hasOwnProperty('change') && " +
                        "$._data($('#clear-completed').get(0), 'events').hasOwnProperty('click')"));
    }


    public static  void givenAtActive(Task... tasks) {
        given(tasks);
        filterActive();
    }

    public static  void givenAtCompleted(Task... tasks) {
        given(tasks);
        filterCompleted();
    }

    public static  Task[] aTask(TaskStatus taskStatus, String... taskTexts) {
        Task[] tasks = new Task[taskTexts.length];

        for (int i = 0; i < taskTexts.length; i++) {
            tasks[i] = new Task(taskStatus, taskTexts[i]);
        }

        return tasks;
    }

    public static  Task aTask(TaskStatus taskStatus, String taskText) {
        return new Task(taskStatus, taskText);
    }

    public static  void ensureUrlIsCorrect() {
        String baseURL = "https://todomvc4tasj.herokuapp.com";

        if (!baseURL.equals(getCurrentUrl())) {
            openUrl(baseURL);
        }
    }
}
